import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory{
    private String fileName;
    private List<Product> products = new ArrayList<>();

    public ProductsRepositoryFileBasedImpl(String filename) {
        this.fileName = filename;
        try(BufferedReader reader = new BufferedReader(new FileReader(filename))){
            while(reader.ready()){
                String[] string = reader.readLine().split("\\|");
                products.add(new Product(Integer.parseInt(string[0]), string[1],
                        Double.parseDouble(string[2]), Integer.parseInt(string[3])));
            }
        }catch (IOException e){
            System.out.println("Ошибка чтения файла: " + e);
        }
    }

    @Override
    public Product findById(Integer id) {
        for(Product product: products){
            if(product.getID() == id){
                return product;
            }
        }
        return null;
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> result = new ArrayList<>();
        for(Product product: products){
            if(product.getName().contains(title)){
                result.add(product);
            }
        }
        return result;
    }

    @Override
    public void update(Product product) {
         try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))){
             for(Product aProduct: products){
                 StringBuilder sb = new StringBuilder();
                 sb.append(aProduct.getID()).append("|").append(aProduct.getName()).append("|")
                         .append(aProduct.getPrice()).append("|").append(aProduct.getCount()).append("\n");
                 writer.write(sb.toString());
             }
         }catch (IOException e){
             System.out.println("Ошибка записи файла: " + e);
         }
     }
}
