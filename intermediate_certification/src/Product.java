public class Product {
    private final int ID;
    private String name;
    private double price;
    private int count;

    public Product(int ID, String name, double price, int count) {
        this.ID = ID;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }
}
